# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure("2") do |config|


  config.vm.define "ceph3" do |ceph3|
    ceph3.vm.box = "bento/centos-7.6"
    ceph3.vm.network "private_network", ip: "192.168.50.13", nic_type: "82540EM"
    ceph3.vm.synced_folder ".", "/vagrant", type: "virtualbox"
    ceph3.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.customize ["storagectl", :id, "--name", "SATA Controller", "--hostiocache", "on"]
      file_to_disk = 'D:/VMS/CEPH03.vdi'
      unless File.exist?(file_to_disk)
        vb.customize ['createhd', '--filename', file_to_disk, '--size', 32 * 1024]
      end
      vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
    end
  ceph3.vm.provision "shell", inline: <<-SHELL
    sudo echo "ceph3" > /etc/hostname
    sudo sed -i -e "s/PermitRootLogin no/PermitRootLogin yes/" /etc/ssh/sshd_config
    sudo service sshd restart
    sudo hostname ceph3
    sudo echo -e "192.168.50.11    ceph1\n192.168.50.12    ceph2\n192.168.50.13    ceph3" >> /etc/hosts
    sudo useradd ceph -m
    sudo echo -e "12345\n12345" | passwd ceph
    sudo echo -e "ceph ALL = (root) NOPASSWD:ALL\nDefaults:ceph !requiretty" > /etc/sudoers.d/ceph
    sudo chmod 0440 /etc/sudoers.d/ceph
    sudo yum update -y
    sudo grub2-set-default 1
    sudo yum -y install mc
  SHELL
    
  end

  config.vm.define "ceph2" do |ceph2|
    ceph2.vm.box = "bento/centos-7.6"
    ceph2.vm.network "private_network", ip: "192.168.50.12", nic_type: "82540EM"
    ceph2.vm.synced_folder ".", "/vagrant", type: "virtualbox"
    ceph2.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.customize ["storagectl", :id, "--name", "SATA Controller", "--hostiocache", "on"]
      file_to_disk = 'D:/VMS/CEPH02.vdi'
      unless File.exist?(file_to_disk)
        vb.customize ['createhd', '--filename', file_to_disk, '--size', 32 * 1024]
      end
      vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
    end
  ceph2.vm.provision "shell", inline: <<-SHELL
    sudo echo "ceph2" > /etc/hostname
    sudo sed -i -e "s/PermitRootLogin no/PermitRootLogin yes/" /etc/ssh/sshd_config
    sudo service sshd restart
    sudo hostname ceph2
    sudo echo -e "192.168.50.11    ceph1\n192.168.50.12    ceph2\n192.168.50.13    ceph3" >> /etc/hosts
    sudo useradd ceph -m
    sudo echo -e "12345\n12345" | passwd ceph
    sudo echo -e "ceph ALL = (root) NOPASSWD:ALL\nDefaults:ceph !requiretty" > /etc/sudoers.d/ceph
    sudo chmod 0440 /etc/sudoers.d/ceph
    sudo yum update -y
    sudo grub2-set-default 1
    sudo yum -y install mc
  SHELL
    
  end

  config.vm.define "ceph1" do |ceph1|
    ceph1.vm.box = "bento/centos-7.6"
    ceph1.vm.network "private_network", ip: "192.168.50.11", nic_type: "82540EM"
    ceph1.vm.synced_folder ".", "/vagrant", type: "virtualbox"
    ceph1.vm.provider "virtualbox" do |vb|
      vb.memory = "2048"
      vb.customize ["storagectl", :id, "--name", "SATA Controller", "--hostiocache", "on"]
      file_to_disk = 'D:/VMS/CEPH01.vdi'
      unless File.exist?(file_to_disk)
        vb.customize ['createhd', '--filename', file_to_disk, '--size', 32 * 1024]
      end
      vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', file_to_disk]
    end
  ceph1.vm.provision "shell", inline: <<-SHELL
    sudo sed -i -e "s/PermitRootLogin no/PermitRootLogin yes/" /etc/ssh/sshd_config
    sudo service sshd restart
    sudo echo "ceph1" > /etc/hostname
    sudo hostname ceph1
    sudo echo -e "192.168.50.11    ceph1\n192.168.50.12    ceph2\n192.168.50.13    ceph3" >> /etc/hosts
    sudo useradd ceph -m
    sudo echo -e "12345\n12345" | passwd ceph
    sudo echo -e "ceph ALL = (root) NOPASSWD:ALL\nDefaults:ceph !requiretty" > /etc/sudoers.d/ceph
    sudo chmod 0440 /etc/sudoers.d/ceph
    sudo echo -e "[ceph-noarch]\nname=Ceph noarch packages\nbaseurl=http://download.ceph.com/rpm-hammer/el7/noarch\nenabled=1\ngpgcheck=1\ntype=rpm-md\ngpgkey=https://download.ceph.com/keys/release.asc" > /etc/yum.repos.d/ceph.repo
    sudo yum update -y
    sudo grub2-set-default 1
    sudo yum install -y mc sshpass ceph-deploy
    sudo mv /etc/yum.repos.d/ceph.repo /etc/yum.repos.d/ceph-deploy.repo
    sudo -iu ceph bash -c 'echo -e "\n\n\n" | ssh-keygen'
    sudo -iu ceph bash -c 'sshpass -p 12345 ssh-copy-id -o StrictHostKeyChecking=no ceph@ceph1'
    sudo -iu ceph bash -c 'sshpass -p 12345 ssh-copy-id -o StrictHostKeyChecking=no ceph@ceph2'
    sudo -iu ceph bash -c 'sshpass -p 12345 ssh-copy-id -o StrictHostKeyChecking=no ceph@ceph3'
    sudo -iu ceph bash -c 'mkdir ceph-admin'
    sudo -iu ceph bash -c 'cd ceph-admin && ceph-deploy new ceph1 ceph2 ceph3'
    sudo -iu ceph bash -c 'cd ceph-admin && ceph-deploy install --release nautilus ceph1 ceph2 ceph3'
    sudo yum install -y ceph-deploy-2.0.1
    sudo -iu ceph bash -c 'cd ceph-admin && ceph-deploy mon create-initial'
    sudo -iu ceph bash -c 'cd ceph-admin && ceph-deploy mds create ceph1 ceph2 ceph3'
    sudo -iu ceph bash -c 'cd ceph-admin && ceph-deploy mgr create ceph1'
    sudo -iu ceph bash -c 'cd ceph-admin && ceph-deploy osd create ceph1 --data /dev/sdb'
    sudo -iu ceph bash -c 'cd ceph-admin && ceph-deploy osd create ceph2 --data /dev/sdb'
    sudo -iu ceph bash -c 'cd ceph-admin && ceph-deploy osd create ceph3 --data /dev/sdb'
    sudo -iu ceph bash -c 'cd ceph-admin && ceph-deploy admin ceph1 ceph2 ceph3'
    sudo ceph config set mon auth_allow_insecure_global_id_reclaim false
    sudo ceph osd pool create cephfs_data 32
    sudo ceph osd pool create cephfs_metadata 32
    sudo ceph fs new cephfs cephfs_metadata cephfs_data
    sudo ceph --cluster ceph osd pool create kube 32 32
    sudo ceph osd pool application enable kube rbd
    sudo ceph --cluster ceph auth get-or-create client.kube mon 'allow r' osd 'allow rwx pool=kube'
  SHELL
    
  end

  config.vm.define "kubernetes" do |kubernetes|
    kubernetes.vm.box = "bento/centos-8.5"
  
    kubernetes.ssh.username = "root"
    kubernetes.ssh.password = "vagrant"
    kubernetes.vm.network "forwarded_port", guest: 30443, host: 30443, host_ip: "127.0.0.1"
    kubernetes.vm.network "forwarded_port", guest: 80, host: 80, host_ip: "127.0.0.1"
    kubernetes.vm.network "forwarded_port", guest: 443, host: 443, host_ip: "127.0.0.1"
    kubernetes.vm.network "private_network", ip: "192.168.50.14"
    kubernetes.vm.synced_folder ".", "/vagrant", type: "virtualbox"
    kubernetes.vm.provider "virtualbox" do |vb|
      vb.memory = "32768"
        vb.customize ["storagectl", :id, "--name", "SATA Controller", "--hostiocache", "on"]
        vb.cpus = 4
  end
  kubernetes.vm.provision "shell", inline: <<-SHELL
    sudo echo "k3-imc" > /etc/hostname
    sudo hostname "k3-imc"
    sudo echo "192.168.50.14    k3-imc" >> /etc/hosts
    sudo echo "192.168.50.11    ceph1" >> /etc/hosts
    sudo echo "192.168.50.12    ceph2" >> /etc/hosts
    sudo echo "192.168.50.13    ceph3" >> /etc/hosts
    sudo sed -i -e "s/PermitRootLogin no/PermitRootLogin yes/" /etc/ssh/sshd_config
    sudo service sshd restart
    # wget 'http://mirror.centos.org/centos/8-stream/BaseOS/x86_64/os/Packages/centos-gpg-keys-8-6.el8.noarch.rpm'
    # sudo rpm -i 'centos-gpg-keys-8-6.el8.noarch.rpm'
    sudo echo "fastestmirror=1" >> /etc/dnf/dnf.conf
    sudo echo "max_parallel_downloads=8" >> /etc/dnf/dnf.conf
    sudo dnf clean all
    sudo dnf -y --disablerepo '*' --enablerepo=extras swap centos-linux-repos centos-stream-repos
    sudo rpm --import https://www.elrepo.org/RPM-GPG-KEY-elrepo.org
    sudo yum -y install https://www.elrepo.org/elrepo-release-8.el8.elrepo.noarch.rpm
    sudo rpm -Uvh http://www.elrepo.org/elrepo-release-7.0-3.el7.elrepo.noarch.rpm 
    ## sudo export VERSION=1.26
    sudo curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable.repo https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable/CentOS_8/devel:kubic:libcontainers:stable.repo
    sudo curl -L -o /etc/yum.repos.d/devel:kubic:libcontainers:stable:cri-o:1.27.repo https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:1.27/CentOS_8/devel:kubic:libcontainers:stable:cri-o:1.27.repo
    sudo yum -y update
    sudo grub2-set-default 1
    sudo yum -y install mc git
    sudo yum -y install cri-o
    sudo yum -y install centos-release-ceph-nautilus
    sudo yum -y install ceph sshpass
    sudo yum install -y kubelet kubectl
    sudo sshpass -p vagrant scp -o StrictHostKeyChecking=no root@ceph1:/etc/ceph/ceph.conf /etc/ceph/
    sudo sshpass -p vagrant scp -o StrictHostKeyChecking=no root@ceph1:/etc/ceph/ceph.client.admin.keyring /etc/ceph/
    ## sudo echo -e "{\\n    \\"insecure-registries\\" : [ \\"kubernetes:30500\\", \\"nexus.kubernetes.lan.home:30500\\", \\"gitlab.kubernetes.lan.home:30600\\" ],\\n    \\"disable-legacy-registry\\": true\\n}" > /etc/docker/daemon.json
    sudo systemctl restart network

    sudo cp /vagrant/kubernetes.repo /etc/yum.repos.d/
    sudo chmod 0644 /etc/yum.repos.d/kubernetes.repo
    sudo setenforce 0
    sudo modprobe br_netfilter ip_tables
    sudo sysctl net.bridge.bridge-nf-call-iptables=1
    sudo sysctl net.bridge.bridge-nf-call-ip6tables=1
    sudo sysctl net.ipv4.ip_forward=1
    sudo echo "net.bridge.bridge-nf-call-iptables=1" >> /etc/sysctl.conf
    sudo echo "net.bridge.bridge-nf-call-ip6tables=1" >> /etc/sysctl.conf
    sudo echo "net.ipv4.ip_forward=1" >> /etc/sysctl.conf
    
    sudo swapoff -v -a
    sudo systemctl enable --now crio
    sudo yum install -y cri-tools kubeadm
    sudo sed -i "/swap/ s/^#*/#/" /etc/fstab
    sudo systemctl enable --now kubelet
    sudo kubeadm reset --force && kubeadm init --pod-network-cidr=10.244.0.0/16
    #sudo kubeadm reset --force && kubeadm init --pod-network-cidr=192.168.0.0/16
    mkdir -p $HOME/.kube
    sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    sudo chown $(id -u):$(id -g) $HOME/.kube/config
    kubectl taint nodes --all node-role.kubernetes.io/control-plane-
    kubectl create clusterrolebinding permissive-binding  --clusterrole=cluster-admin --user=admin --user=kubelet --group=system:serviceaccounts
    ##kubectl apply -f /vagrant/kube-flannel.yml
    kubectl create -f /vagrant/canal.yaml
    #kubectl create -f /vagrant/tigera-operator.yaml
    #kubectl create -f /vagrant/custom-resources.yaml
    kubectl create -f /vagrant/metrics-server.yaml
    kubectl create -f /vagrant/kubernetes-dashboard-recommended.yaml
    sudo /bin/bash /vagrant/podswaiting.sh
    sudo cd /root
    sudo wget https://get.helm.sh/helm-v3.11.3-linux-amd64.tar.gz
    sudo tar -zxvf helm-v3.11.3-linux-amd64.tar.gz
    sudo cp linux-amd64/helm /usr/local/bin/
    sudo /usr/local/bin/helm repo add traefik https://helm.traefik.io/traefik
    sudo /usr/local/bin/helm repo add ceph-csi https://ceph.github.io/csi-charts
    sudo /usr/local/bin/helm repo update
    sudo /usr/local/bin/helm install traefik traefik/traefik -f /vagrant/traefik_values.yaml --namespace=kube-system
    sudo yes | cp -f /vagrant/cephfs_values.yml /tmp/
    sudo yes | cp -f /vagrant/cephrbd_values.yml /tmp/
    sudo sed -i "s/{{ cluster_id }}/$(ceph fsid)/g" /tmp/cephrbd_values.yml
    sudo sed -i "s,{{ kube_key }},$(ceph auth get-key client.kube),g" /tmp/cephrbd_values.yml
    sudo sed -i "s/{{ cluster_id }}/$(ceph fsid)/g" /tmp/cephfs_values.yml
    sudo sed -i "s,{{ admin_key }},$(ceph auth get-key client.admin),g" /tmp/cephfs_values.yml
    sudo /usr/local/bin/helm upgrade -i ceph-csi-rbd ceph-csi/ceph-csi-rbd -f /tmp/cephrbd_values.yml -n ceph --create-namespace
    sudo /usr/local/bin/helm upgrade -i ceph-csi-cephfs ceph-csi/ceph-csi-cephfs -f /tmp/cephfs_values.yml -n ceph --create-namespace
  SHELL
  end
end
