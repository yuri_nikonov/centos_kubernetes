#!/bin/bash

RUNNINGPODS=(`kubectl --namespace="kube-system" get pods --no-headers | cut -f 1 -d ' '`)
while [[ ${#RUNNINGPODS[*]} -lt 7 ]]
do
    echo "Present pods:"
    for item in ${RUNNINGPODS[*]}; do
        echo "- $item"
    done
    sleep 1
    RUNNINGPODS=(`kubectl --namespace="kube-system" get pods --no-headers | cut -f 1 -d ' '`)
done

RUNNINGPODS=(`kubectl --namespace="kube-system" get pods --no-headers | grep -v Running | cut -f 1 -d ' '`)
while [[ ${#RUNNINGPODS[*]} -ne 0 ]]
do
    echo "Running pods:"
    for item in ${RUNNINGPODS[*]}; do
        echo "- $item"
    done
    sleep 1
    RUNNINGPODS=(`kubectl --namespace="kube-system" get pods --no-headers | grep -v Running | cut -f 1 -d ' '`)
done

